const Chopin = require('./chopin');
const tools = require('./tools');

module.exports = new Chopin().use(tools);