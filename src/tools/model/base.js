const Base = (baseclass = class { }) =>
    class extends baseclass {
        constructor(...args) {
            super(...args);
            this.features = this.features || {};
        }
    }

function initBase({ initializers, args }) {
    const initMethods = ({ methods }) => Object.entries(methods)
        .forEach(([name, method]) => name && !this[name] && Object.assign(this, {
            [name]: method.bind(this)
        }));

    const runInitializers = ({ initializers }) => Object.values(initializers)
        .forEach(initializer => initializer.call(this))

    const features = Object.values(this.features).sort((a, b) => a.id - b.id);

    features.forEach(initMethods);
    features.forEach(runInitializers);
    initializers.forEach(i => i.call(this, ...args));

    return this;
}

module.exports = { Base, initBase }