module.exports = () => class {
    constructor(base) {
        this.base = base;
        this.overloads = [];
    }

    on(args = [], method = function () { }, spread) {
        this.overloads.push([args.map(toDetectionMethod), method, spread]);
        return this;
    }

    generate() {
        const { overloads, base } = this;
        return function (...args) {
            const [, overload] = overloads.find(([detectionMethods, , spread]) => {
                return (spread || detectionMethods.length === args.length)
                    && detectionMethods.every((m, idx) => m(args[idx]))
                    && (!spread || args.every((arg, idx) => idx < detectionMethods.length ||
                        detectionMethods[detectionMethods.length - 1](arg)))
            }) || [];
            return overload
                ? overload(...args)
                : base(...args);
        }
    }
}

const toDetectionMethod = arg => val => {
    if (!val) return false;
    if (Array.isArray(arg)) return arg.map(toDetectionMethod).some(m => m(val));

    switch (typeof arg) {
        case 'function': return val.constructor.name === arg.name;
        case 'object': return Object.entries(arg)
            .map(([key, type]) => [key, toDetectionMethod(type)])
            .every(([key, validation]) => validation(val[key]))
        case 'string': if (arg === '*') return true;
            return arg.charAt(0) === ':'
                ? toProtoChain(val).includes(arg.replace(':', ''))
                : val.constructor.name === arg;
        default: throw Error('unsupported arg type');
    }
}

const toProtoChain = (arg, result = []) => arg.__proto__
    ? toProtoChain(arg.__proto__, [...result, arg.constructor.name])
    : result.filter((val, idx, src) => src.indexOf(val) === idx)