
const { _Object } = require('chopin-methods');

module.exports = class Chopin {
    constructor(tools = {}) { this.tools = tools; }
    clone() { return new Chopin(this.tools); }

    compose(cb) {
        return cb(_Object.pipe(this.tools, tools => tools
            .mapValues(tool => (...args) => new tool(...args))))
            .generate();
    }

    build({ features, models }) {
        const buildFeatues = features => _Object.mapValues(features, feature => (...args) => this.compose(feature(...args)));
        return Object.values(models).map(model => this.compose(model(buildFeatues(features))));
    }

    use(arg1, ...rest) {
        handleByType(arg1, {
            object: objcetHandler.bind(this),
            function: funcHandler.bind(this),
            string: stringHandler.bind(this)
        })(arg1, ...rest);

        return this;
    }
};

function stringHandler(name, func) {
    if (!func) throw Error(`string handler ${name} func is undefined`);
    else this.tools[name] = func(this.tools);
};

function funcHandler(func) {
    if (!func.name) throw Error(`function handler must accept named function`);
    else stringHandler.call(this, func.name, func);
}

function objcetHandler(obj) {
    Object.entries(obj)
        .forEach(([key, builder]) => stringHandler.call(this, key, builder))
}

const handleByType = (arg, typeHandles = {}) => typeHandles[typeof (arg)];