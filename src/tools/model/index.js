const { _Function } = require('chopin-methods');
const { Base, initBase } = require('./base')

module.exports = () => class ModelPlanner {
    constructor(config) {
        construct.call(this, config);

        this.features = [];
        this.constructions = [];
        this.initializers = []
    }

    from(...features) {
        this.features = [...this.features, spreadArray(features, [])];
        return this;
    }

    on(baseclass = class { }) {
        this.baseclass = Base(baseclass);
        return this;
    }

    construct(...construction) {
        this.constructions.push(...construction);
        return this;
    }

    initialize(...initializers) {
        this.initializers.push(...initializers);
        return this;
    }

    generate() {
        const { baseclass, features, name, constructions, initializers } = this;
        return {
            [name]: new Proxy(class extends _Function.compose(...[...features, baseclass]) {
                constructor(...args) {
                    super(...args);
                    constructions.forEach(construction => construction.call(this, ...args));
                }
            }, {
                construct: (target, args) => initBase.call(new target(...args), {
                    initializers, args
                })
            })
        }[name];
    }
}

const spreadArray = (acc, current) => Array.isArray(current) ? current.reduce(spreadArray, acc).pop() : [...acc, current];

function construct(arg) {
    switch (typeof arg) {
        case 'string':
            this.name = arg;
            this.on();
            break;
        case 'function':
            this.name = arg.name;
            this.on(arg)
            break;
        default:
            this.name = '';
            this.on();
            break;
    }
}