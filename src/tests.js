const Chopin = require('./index');
const { _String } = require('chopin-methods');

try {
    //clone for later ref
    const chopinClone = Chopin.clone();

    /** Use Wrapper */
    /** custom genral tools */
    const construction = (attribute, initialValue) => function (data = {}) { this[attribute] = data[attribute] || initialValue }
    const getter = attribute => function () { return this[attribute] }
    const setter = attribute => function (value) { this[attribute] = value }
    const baseFeature = feature => feature.appendData({ baseFeatureData: {} })

    const generalTools = {
        attribute: ({ feature }) => class Attribute extends feature {
            constructor(name, initialValue, ...rest) {
                super(`${name}able`, initialValue, ...rest);
                this.on(baseFeature)
                    .addMethod(`get${_String.capitalize(name)}`, getter(name))
                    .addMethod(`set${_String.capitalize(name)}`, setter(name))
                    .construct(construction(name, initialValue))
            }
        }
    }

    /** custom api tools */
    const apiTools = {
        route: ({ feature }) => class extends feature {
            constructor(name, method, ...args) {
                super(name, method, ...args);
                this.on(feature => feature.appendData({ items: [] }))
                    .addMethod(method)
            }

            addMethod(method) { return super.addMethod(this.name, method); }
            addMethods() { throw Error('route supports only single methods') }
        },
        controller: ({ model }) => class extends model {
            constructor(name, ...rest) {
                super(name, ...rest);
                this.from(
                    Chopin.compose(({ feature }) => feature('baseController')
                        .addMethod(function parseRequest({ body }) { console.log(body) }))
                )
            }
        }
    }

    /** extend Chopin toolkit */
    Chopin
        .use(apiTools)
        .use(generalTools);

    const personFeatures = [
        /** custom tool from feature*/
        Chopin.compose(({ attribute }) => attribute('firstName')),
        Chopin.compose(({ attribute }) => attribute('lastName')),
        Chopin.compose(({ attribute }) => attribute('ageName', 0)),
        /** feature composition */
        // post construct
        Chopin.compose(({ feature }) => feature('postConstruct').initialize(function () { console.log('i was made post construct. this is ME:', this) })),
        // on construct
        Chopin.compose(({ feature }) => feature('onConstruct').construct(function () { console.log('i was made on construct. this is ME:', this) })),
        // method extension
        Chopin.compose(({ feature, overload }) => feature('Sir')
            .addMethod(function setFirstName(name) { this.features.firstNameable.methods.setFirstName(`Sir ${name}`); })
            .addMethod('fight', overload(() => console.log('nothing to fight with...'))
                .on([String], weapon => console.log(`ill use ${weapon} as a weapon!`))
                .on(['*'], val => console.log('you know its not a weapon...'))
                .generate(),
            ))
    ]

    const controllerFeatures = [
        /** custom tool from func*/
        Chopin.compose(({ route }) => route('getAll', function () { console.log('getting all!'); })),
    ]

    /** class composition */
    const OnClass = Chopin.compose(({ model }) =>
        model(class FromClass {
            constructor(...args) { }
            print(...args) { console.log(...args) }
        })
    )
    const Person = Chopin.compose(({ model }) => model('Person').from(personFeatures));
    const Controller = Chopin.compose(({ controller }) => controller('Users')
        .from(controllerFeatures))

    /** class initialization */
    const p = new Person();
    const c = new Controller('Users');

    const onClass = new OnClass();

    onClass.print('i was made from class defenition');

    c.getAll();
    c.parseRequest({ body: 'helloWorld! im body!' })
    p.setFirstName('assaf');

    p.fight()
    p.fight('sword');
    p.fight(4);
    console.log('p.firstName: ', p.firstName);
    console.log('choin-clone', chopinClone.tools);
    console.log('choin-orige', Chopin.tools);

    /** overloading */
    const o = Chopin.compose(({ overload }) => overload(console.log)
        .on([String], string => console.log(`ITS A STRING: ${string}`))
        .on([Number], string => console.log(`ITS A NUMBER: ${string}`)))

    o('a');
    o(4);
}
catch (e) {
    console.error('--FAILD ON TESTS');
    throw e;
}