const Base = require('./base');
const Overload = require('../overload')();
const { _Object } = require('chopin-methods');

module.exports = () => class FeaturePlanner {
    constructor(name) {
        this.name = name;
        this.constructions = [];
        this.initializers = [];
        this.data = {};
        this.methods = {};

        this.baseFeature = feature => feature;
    }

    addMethod(key, method) {
        this.methods[key.name || key] = method || key;
        return this;
    }

    addMethods(methods) {
        Object.assign(this.methods, methods)
        return this;
    }

    on(baseFeature) {
        this.baseFeature = baseFeature;
        return this;
    }

    appendData(key, value) {
        if (value) _Object.deepSet(this.data, key, value);
        else Object.assign(this.data, key);

        return this;
    }

    setData(data) {
        this.data = data;
        return this;
    }

    construct(...constructors) {
        this.constructions.push(...constructors);
        return this;
    }

    initialize(...initializers) {
        this.initializers.push(...initializers);
        return this;
    }

    generate() {
        return Base(this.baseFeature(this))
    }
}