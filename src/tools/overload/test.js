const Overload = require('../overload')();

class Abstract { constructor() { this.state = 'abstract_state' } }
class Person extends Abstract { seyHey() { console.log('hey! im a person!') } }
class Persona extends Abstract { }

const o = new Overload(function (arg = 'no args!') { console.log(arg); })
    // primitive match
    .on([String], string => console.log(string + ' modified..'))
    // class match
    .on([Person], person => person.seyHey())
    // 3rd arg for spread (...) operator
    .on([String], () => console.log('lots of stringzzzzz'), true)
    // support for multiple types (or)
    .on([[String, Number], [String, Number]], (a, b) => console.log(parseInt(a) + parseInt(b)))
    // decorate with : for match by inharitance
    .on([':Abstract'], a => console.log(a.state))
    // match Array
    .on([Array], items => console.log('what a funky array!!'))
    // match anonymos object
    .on([{ id: Number, person: ':Abstract' }], ({ id, person }) => console.log(`${id} ${person.state}`))
    .generate();

o(new Person());                        // hey! im a person
o('its so');                            // its so modified..
o('will it', 'work?')                   // lots of stringzzzzz
o(4);                                   // 4
o('hello', 4);                          // NaN
o(new Persona())                        // abstract_state
o();                                    // no args!
o([]);                                  // what a funky array!!
o({ id: 27, person: new Person })       // 27 abstract_state

console.log('OVERLOAD TEST SUCCESS!');