const { _Object } = require('chopin-methods');

module.exports = ({ name, constructions = [], data = {}, methods = {}, initializers }) =>
    (superclass, idx) => class extends superclass {
        constructor(...args) {
            super(...args);
            this.features = this.features || {};

            if (this.features[name]) throw Error(`feature by ${name} already exist`)

            Object.assign(this.features, {
                [name]: Object.assign(data, {
                    idx, initializers,
                    methods: _Object.mapValues(methods, method => method.bind(this))
                })
            });

            constructions.forEach(construct => construct.call(this, ...args));
        }
    }